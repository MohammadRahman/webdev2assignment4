"use strict";

!(function() { 

    const EpicApplication = {
        imageData: [],
        selectedImageType: '',
        dateCache: {},
        imageCaches: {},
    };

    const imageTypes = ["natural", "enhanced", "aerosol", "cloud"];

    imageTypes.forEach((type) => {
        EpicApplication.dateCache[type] = null;
        EpicApplication.imageCaches[type] = new Map();
    });

    document.addEventListener("DOMContentLoaded", function(e) {

        //let imageData = [];

        const imageMenu = document.getElementById('image-menu');
        const template = document.getElementById('image-menu-item');

        function showErrorMessage(message){
            imageMenu.innerHTML = '';
            const listItem = document.importNode(template.content, true);
            listItem.firstElementChild.textContent = message;
            imageMenu.appendChild(listItem);
        }

            document.getElementById('request_form').addEventListener("submit", fetchData);
                
                function fetchData(event) {
                event.preventDefault();


                EpicApplication.selectedImageType = document.getElementById('type').value;
                let date = document.getElementById('date').value;


                // Check the image cache for the selected date
                const imageCache = EpicApplication.imageCaches[EpicApplication.selectedImageType];
                if (imageCache.has(date)) {
                // If there is a cache hit, use the cached data
                EpicApplication.imageData = Array.from(imageCache.get(date));
                renderImages();
                return;
                }

                function renderImages() {

                    imageMenu.innerHTML = '';
              
                    EpicApplication.imageData.forEach((image, index) => {
                    const listItem = document.importNode(template.content, true);
                    listItem.querySelector('.image-list-title').textContent = image.date;
                    listItem.firstElementChild.addEventListener('click', () => displayImage(index));
                    imageMenu.appendChild(listItem);
                })}


                fetch(`https://epic.gsfc.nasa.gov/api/${EpicApplication.selectedImageType}/date/${date}`)
                    .then( response => { 
                        
                        if( !response.ok ) { 
                            throw new Error("Not 2xx response", {cause: response});
                        }
                        
                        return response.json();
                    })

                    .then( obj => {

                        EpicApplication.imageData = obj;
                

                        if (EpicApplication.imageData.length === 0) {
                            showErrorMessage('No images found for the selected date.');
                            return;
                        }

                        EpicApplication.dateCache[EpicApplication.selectedImageType] = date;
                        const imageCache = EpicApplication.imageCaches[EpicApplication.selectedImageType];
                        imageCache.set(date, EpicApplication.imageData);
                        console.log("EpicApplication after updating caches:", EpicApplication);
                        imageMenu.innerHTML = '';

                        EpicApplication.imageData.forEach((image, index) => {
                        const listItem = document.importNode(template.content, true);
                        listItem.querySelector('.image-list-title').textContent = image.date;
                        listItem.firstElementChild.addEventListener('click', () => displayImage(index));
                        imageMenu.appendChild(listItem);

                        function displayImage(index) {
                        const dates = date.split("-");
                        const selectedImage = EpicApplication.imageData[index];
                        document.getElementById('earth-image').src = `https://epic.gsfc.nasa.gov/archive/${EpicApplication.selectedImageType}/${dates[0]}/${dates[1]}/${dates[2]}/jpg/${selectedImage.image}.jpg`;
                        document.getElementById('earth-image-date').textContent = selectedImage.date;
                        document.getElementById('earth-image-title').textContent = selectedImage.caption;
                        }
                    });

                        function initializeDate(imageType) {

                            // Check the cache before making a network request
                            if (EpicApplication.dateCache[EpicApplication.selectedImageType] !== null) {
                            // If the date is in the cache, set it immediately and exit the function
                            document.getElementById('date').value = EpicApplication.dateCache[EpicApplication.selectedImageType];
                            fetchData(event);
                            return;
                            }

                            fetch(`https://epic.gsfc.nasa.gov/api/${imageType}/all`)
                            .then( response => {   
                                return response.json();
                            })
        
                            .then( obj => {
                                event.preventDefault();
                                const sortedData = obj.sort((a, b) => b - a);
                                document.getElementById('date').value = sortedData[0].date;
                                fetchData(event);
                            });
    
                        }

                        document.getElementById('type').addEventListener('change', function () {
                            initializeDate(document.getElementById('type').value);
                        });  
                        
                    })

                    .catch( err => {
                        showErrorMessage('An error occured, please enter a valid date.');
                    });

                }

    });
}());